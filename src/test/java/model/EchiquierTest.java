package model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * Created by milihhard on 12/06/17.in model
 */
public class EchiquierTest {

    Echiquier e;
    public EchiquierTest(){
        e = new Echiquier();
    }

    @Test
    public void getColorCurrentPlayer() {
        assertEquals(e.getColorCurrentPlayer(), Couleur.BLANC);
    }

    @Test
    public void getMessage() {

    }

    @Test
    public void getPieceColor() {
      assertEquals(e.getPieceColor(0, 0), Couleur.NOIR);
      assertEquals(e.getPieceColor(4, 1), Couleur.NOIR);
      assertEquals(e.getPieceColor(3, 7), Couleur.BLANC);
      assertEquals(e.getPieceColor(2, 6), Couleur.BLANC);
    }

    @Test
    public void isEnd() {

    }

    @Test
    public void isMoveOk() {
        /*
      // pion noir n 4 se déplace de 2
      assertTrue(e.isMoveOk(3,1,3,3));
      // pion noir n 3 se déplace de 1
      assertTrue(e.isMoveOk(2,1,3,2));
      // cavalier blanc n 1 se déplase an haut a droite
      assertTrue(e.isMoveOk(1,7,2,5));
      // reine blanche tante de se déplacé en haut de 1 case
      assertTrue(e.isMoveOk(3,7,3,6));
      // reine banche tante de se déplacé en haut de 3 case 
      assertTrue(e.isMoveOk(3,7,3,4));
      */
    }

    @Test
    public void move() {

    }

    @Test
    public void switchJoueur() {

    }

    @Test
    public void showEchiquier() {

    }

}
