package model.piece;

import model.Coord;
import model.Couleur;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
/**
 * Created by milihhard on 12/06/17.in model.piece
 */
public class CavalierTest {

    Cavalier c;
    public CavalierTest(){
        c = new Cavalier(Couleur.BLANC, new Coord(5,5));
    }

    @Test
    public void canTheoricalyMove() {
      assertTrue(c.canTheoricalyMove(new Coord(6,3)));
      assertFalse(c.canTheoricalyMove(new Coord(5,3)));
    }

    @Test
    public void getNextStep() {
      assertEquals(c.getNextStep(c.getPos(), new Coord(6,3)), new Coord(6,3));
    }

}
