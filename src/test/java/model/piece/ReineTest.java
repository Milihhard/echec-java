package model.piece;

import model.Coord;
import model.Couleur;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
/**
 * Created by milihhard on 12/06/17.in model.piece
 */
public class ReineTest {

    Reine r;
    public ReineTest(){
        r = new Reine(Couleur.BLANC, new Coord(5,5));
    }

    @Test
    public void canTheoricalyMove() {
      // dayagonal movmont test
      assertTrue(r.canTheoricalyMove(new Coord(7,7)));
      assertFalse(r.canTheoricalyMove(new Coord(7,6)));
      // strait movmont test
      assertTrue(r.canTheoricalyMove(new Coord(2,5)));
      assertFalse(r.canTheoricalyMove(new Coord(7,4)));

    }

    @Test
    public void getNextStep() {
      assertEquals(r.getNextStep(r.getPos(), new Coord(1,1)), new Coord(4,4));
      assertEquals(r.getNextStep(new Coord(6,6), new Coord(8,8)), new Coord(7,7));

      assertEquals(r.getNextStep(r.getPos(), new Coord(10,5)), new Coord(6,5));
      assertEquals(r.getNextStep(new Coord(6,5), new Coord(10,5)), new Coord(7,5));
    }

}
