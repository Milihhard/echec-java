package model.piece;

import model.Coord;
import model.Couleur;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
/**
 * Created by milihhard on 12/06/17.in model.piece
 */
public class PionTest {

    Pion pb, pn, pn2, pb2;

    public PionTest(){
        pb = new Pion(Couleur.BLANC, new Coord(5,5));
        pn = new Pion(Couleur.NOIR, new Coord(5,5));
        pb2 = new Pion(Couleur.BLANC, new Coord(3,6));
        pn2 = new Pion(Couleur.BLANC, new Coord(3,4));
    }
    @Test
    public void canTheoricalyMove() {
        /*
        assertTrue(pb.canTheoricalyMove(new Coord(5,4)));
        assertFalse(pb.canTheoricalyMove(new Coord(5,6)));
        assertTrue(pb.canTheoricalyMove(new Coord(6,4)));
        assertFalse(pb.canTheoricalyMove(new Coord(4,6)));
        assertFalse(pn.canTheoricalyMove(new Coord(5,4)));
        assertTrue(pn.canTheoricalyMove(new Coord(5,6)));
        assertFalse(pn.canTheoricalyMove(new Coord(6,4)));
        assertTrue(pn.canTheoricalyMove(new Coord(4,6)));
        assertTrue(pb2.canTheoricalyMove(new Coord(3,4)));
        assertFalse(pb2.canTheoricalyMove(new Coord(3,3)));
        assertTrue(pn2.canTheoricalyMove(new Coord(4,3)));
        */
    }

    @Test
    public void getNextStep() {
        assertEquals(pb.getNextStep(pb.getPos(), new Coord(5,6)), new Coord(5,6));
        assertEquals(pb.getNextStep(pb.getPos(), new Coord(4,6)), new Coord(4,6));
        assertEquals(pn.getNextStep(pn.getPos(), new Coord(5,4)), new Coord(5,4));
        assertEquals(pn.getNextStep(pn.getPos(), new Coord(6,4)), new Coord(6,4));
    }

    @Test
    public void move() {
        assertTrue(pn2.move(new Coord(3,4)));
    }

}