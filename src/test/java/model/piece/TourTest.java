package model.piece;

import model.Coord;
import model.Couleur;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
/**
 * Created by milihhard on 12/06/17.in model.piece
 */
public class TourTest {

    Tour t;
    public TourTest(){
        t = new Tour(Couleur.BLANC, new Coord(5,5));
    }

    @Test
    public void canTheoricalyMove() {
        assertTrue(t.canTheoricalyMove(new Coord(10,5)));
        assertFalse(t.canTheoricalyMove(new Coord(3,2)));
    }

    @Test
    public void getNextStep() {
        assertEquals(t.getNextStep(t.getPos(), new Coord(10,5)), new Coord(6,5));
        assertEquals(t.getNextStep(t.getPos(), new Coord(5,10)), new Coord(5,6));
        assertEquals(t.getNextStep(new Coord(6,5), new Coord(10,5)), new Coord(7,5));
    }

}