package model.piece;

import model.Coord;
import model.Couleur;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
/**
 * Created by milihhard on 12/06/17.in model.piece
 */
public class FouTest {

    Fou f;
    public FouTest(){
        f = new Fou(Couleur.BLANC, new Coord(5,5));
    }

    @Test
    public void canTheoricalyMove() {
      assertTrue(f.canTheoricalyMove(new Coord(7,7)));
      assertFalse(f.canTheoricalyMove(new Coord(6,5)));
    }

    @Test
    public void getNextStep() {
      assertEquals(f.getNextStep(f.getPos(), new Coord(1,1)), new Coord(4,4));
      assertEquals(f.getNextStep(new Coord(6,6), new Coord(8,8)), new Coord(7,7));
    }

}
