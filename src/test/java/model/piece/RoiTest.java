package model.piece;

import model.Coord;
import model.Couleur;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
/**
 * Created by milihhard on 12/06/17.in model.piece
 */
public class RoiTest {

    Roi r;
    public RoiTest(){
        r = new Roi(Couleur.BLANC, new Coord(5,5));
    }

    @Test
    public void canTheoricalyMove() {
      // dayagonal movmont test
      assertTrue(r.canTheoricalyMove(new Coord(6,6)));
      assertFalse(r.canTheoricalyMove(new Coord(7,6)));
      // strait movmont test
      assertTrue(r.canTheoricalyMove(new Coord(4,5)));
      assertFalse(r.canTheoricalyMove(new Coord(7,4)));
    }

    @Test
    public void getNextStep() {
      assertEquals(r.getNextStep(r.getPos(), new Coord(4,4)), new Coord(4,4));
    }

}
