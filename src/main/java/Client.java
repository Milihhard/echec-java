import model.Couleur;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by milihhard on 14/06/17.in PACKAGE_NAME
 */
public class Client extends LauncherSocket {


    public Client() {
        super(Couleur.NOIR);
    }

    @Override
    Socket recieveSocket() {
        Socket s = null;
        try {

            System.out.println("Demande de connexion");
            s = new Socket("127.0.0.1", 2009);
            System.out.println("Connexion établie avec le serveur, authentification :"); // Si le message s'affiche c'est que je suis connect�


        } catch (UnknownHostException e) {
            System.err.println("Impossible de se connecter à l'adresse " + socket.getLocalAddress());
        } catch (IOException e) {
            System.err.println("Aucun serveur à l'écoute du port " + socket.getLocalPort());
        }
        return s;
    }
}
