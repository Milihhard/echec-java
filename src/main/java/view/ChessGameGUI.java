package view;

import controler.ChessGameControlers;
import controler.controlerLocal.ChessGameControler;
import model.*;
import tools.ChessImageProvider;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.*;
import java.util.List;

/**
 * Created by milihhard on 13/06/17.in view
 */
public class ChessGameGUI extends JFrame implements MouseListener,MouseMotionListener, Observer {

    JLayeredPane layeredPane;
    JPanel chessBoard;
    JLabel chessPiece;
    int xAdjustment;
    int yAdjustment;
    Dimension boardSize;
    ChessGameControlers chessGameControlers;
    List<JLabel> piecesLabel;
    int xInit,yInit;
    JLabel message;

    public ChessGameGUI(String s, ChessGameControlers chessGameControler, Dimension dim) {
        this.boardSize = dim;
        this.boardSize.height-=25;
        System.out.println(boardSize);
        this.chessGameControlers = chessGameControler;

        //  Use a Layered Pane for this this application
        layeredPane = new JLayeredPane();
        getContentPane().add(layeredPane);
        layeredPane.setPreferredSize(dim);
        layeredPane.addMouseListener(this);
        layeredPane.addMouseMotionListener(this);
    }


    private void init(List<PieceIHM> pieces){

        initBoard();
        initPieces(pieces);
    }

    private void initBoard(){
        //Add a chess board to the Layered Pane

        layeredPane.removeAll();
        chessBoard = new JPanel();
        layeredPane.add(chessBoard, JLayeredPane.DEFAULT_LAYER);
        chessBoard.setLayout( new GridLayout(8, 8) );
        chessBoard.setPreferredSize( boardSize );
        chessBoard.setBounds(0, 0, boardSize.width, boardSize.height-25);

        for (int i = 0; i < 64; i++) {
            piecesLabel = new ArrayList<>();
            JPanel square = new JPanel( new BorderLayout() );
            chessBoard.add( square );

            int row = (i / 8) % 2;
            if (row == 0)
                square.setBackground( i % 2 == 0 ? Color.black : Color.white );
            else
                square.setBackground( i % 2 == 0 ? Color.white : Color.black );
        }
        message = new JLabel(chessGameControlers.getMessage());
        layeredPane.add(message);
    }
    private void initPieces(List<PieceIHM> pieces){
        //Add a few pieces to the board
        piecesLabel.clear();
        for(PieceIHM piece : pieces){
            piecesLabel.add(new JLabel( new ImageIcon(ChessImageProvider.getImageFile(piece.getTypePiece(), piece.getCouleur()))));
            JPanel panel = (JPanel)chessBoard.getComponent(8 * piece.getY() + piece.getX());
            panel.add(piecesLabel.get(piecesLabel.size() - 1));
        }
        this.pack();
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        chessPiece = null;
        Component c =  chessBoard.findComponentAt(e.getX(), e.getY());

        if (c instanceof JPanel)
            return;

        Point parentLocation = c.getParent().getLocation();
        xAdjustment = parentLocation.x - e.getX();
        yAdjustment = parentLocation.y - e.getY();
        int xBoard = e.getX()/(boardSize.width / 8);
        int yBoard = e.getY()/(boardSize.height / 8);
        if(chessGameControlers.isPlayerOK(new Coord(xBoard, yBoard))) {
            //chessBoard.getComponent(12).setBackground(Color.blue);
            List<Coord> coords = chessGameControlers.getPossibleMove(new Coord(xBoard, yBoard));
            for(Coord coord : coords){
                Component component = chessBoard.getComponent(8 * coord.y + coord.x);
                if(component.getBackground() == Color.black)
                    ((JPanel)component).setBorder(BorderFactory.createLineBorder(new Color(51,51,255), 5));
                else
                    ((JPanel)component).setBorder(BorderFactory.createLineBorder(new Color(25, 25, 127), 5));
            }
            xInit = xBoard;
            yInit = yBoard;
            chessPiece = (JLabel) c;
            chessPiece.setLocation(e.getX() + xAdjustment, e.getY() + yAdjustment);
            chessPiece.setSize(chessPiece.getWidth(), chessPiece.getHeight());
            layeredPane.add(chessPiece, JLayeredPane.DRAG_LAYER);
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {

        int xBoard = e.getX()/(boardSize.width / 8);
        int yBoard = e.getY()/(boardSize.height / 8);
        chessGameControlers.move(new Coord(xInit, yInit), new Coord(xBoard, yBoard));

        /*if(chessPiece == null) return;

        chessPiece.setVisible(false);
        Component c =  chessBoard.findComponentAt(e.getX(), e.getY());

        if (c instanceof JLabel){
            Container parent = c.getParent();
            parent.remove(0);
            parent.add( chessPiece );
        }
        else {
            Container parent = (Container)c;
            parent.add( chessPiece );
        }

        chessPiece.setVisible(true);*/
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseDragged(MouseEvent me) {
        if (chessPiece == null) return;
        chessPiece.setLocation(me.getX() + xAdjustment, me.getY() + yAdjustment);
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {

    }

    @Override
    public void update(Observable observable, Object o) {

        if(o instanceof String ){
            System.out.println("l'autre s'est déconnecté");
            JOptionPane.showMessageDialog(this,
                    "L'autre joueur s'est déconnecté.");
            System.exit(1);
        }else {
            List<PieceIHM> piecesIHM = (List<PieceIHM>) o;
            init(piecesIHM);
        }
    }
}
