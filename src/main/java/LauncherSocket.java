import controler.ChessGameControlerSocket;
import controler.ChessGameControlers;
import model.Couleur;
import model.observable.ChessGame;
import tools.socket.CheckConnexion;
import view.ChessGameGUI;

import javax.swing.*;
import java.awt.*;
import java.net.Socket;
import java.util.Observer;

/**
 * Created by milihhard on 14/06/17.in PACKAGE_NAME
 */
public abstract class LauncherSocket {
    public Socket socket = null;
    private Couleur color;

    public LauncherSocket(Couleur color){
        this.color = color;
    }

    public void go() {
        ChessGame chessGame;
        ChessGameControlers chessGameControler;
        JFrame frame = null;
        Dimension dim;

        socket = recieveSocket();
        if (socket != null) {
            //Thread t1 = new Thread(new CheckConnexion(socket, frame));
            //t1.start();
            dim = new Dimension(700, 725);

            chessGame = new ChessGame();
            chessGameControler = (ChessGameControlers) new ChessGameControlerSocket(chessGame, color, socket);

            frame = new ChessGameGUI("Jeu d'échec", chessGameControler, dim);
            chessGame.addObserver((Observer) frame);

            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setLocation(600, 10);
            frame.setPreferredSize(dim);
            frame.pack();
            frame.setVisible(true);
        }


    }

    /**
     * gère la connexion entre client et serveur et récupère le Socket
     * @return Socker
     */
    abstract Socket recieveSocket();
}
