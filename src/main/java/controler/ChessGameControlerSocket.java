package controler;

import controler.controlerLocal.AbstractChessGameControler;
import model.Coord;
import model.Couleur;
import model.observable.ChessGame;
import tools.socket.RecieveMove;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by milihhard on 14/06/17.in controler
 */
public class ChessGameControlerSocket extends AbstractChessGameControler {
    Couleur colorPlayer;
    Socket socket;

    public ChessGameControlerSocket(ChessGame chessGame, Couleur colorPlayer, Socket socket) {
        super(chessGame);
        this.colorPlayer = colorPlayer;
        this.socket = socket;

        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            Thread t1 = new Thread(new RecieveMove(in, this));
            t1.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean isPlayerOK(Coord initCoord) {
        return colorPlayer.equals(chessGame.getColorCurrentPlayer()) && chessGame.getColorCurrentPlayer().equals(chessGame.getPieceColor(initCoord.x, initCoord.y));
    }

    @Override
    protected void endMove(Coord initCoord, Coord finalCoord, String promotionType) {
        PrintWriter out = null;
        try {
            out = new PrintWriter(socket.getOutputStream());
            //xInit,yInit-xDest,yDest
            out.println(Integer.toString(initCoord.x) + "," +Integer.toString(initCoord.y) + "," + Integer.toString(finalCoord.x) + "," + Integer.toString(finalCoord.y));
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void recieveMessage(String str){
        int xInit, yInit, xDest, yDest;
        //System.out.println(str);
        if(str == null)
            chessGame.notifyObservers("error");
        else if(!"test".equals(str)) {
            String[] split = str.split(",");
            xInit = Integer.parseInt(split[0]);
            yInit = Integer.parseInt(split[1]);
            xDest = Integer.parseInt(split[2]);
            yDest = Integer.parseInt(split[3]);

            //parse string
            chessGame.move(xInit, yInit, xDest, yDest);
        }
    }


}
