package tools.socket;

import controler.ChessGameControlerSocket;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by milihhard on 14/06/17.in tools.socket
 */
public class RecieveMove implements Runnable {

    private BufferedReader in;
    private String message = null;
    private ChessGameControlerSocket chessGameControlerSocket;

    public RecieveMove(BufferedReader in, ChessGameControlerSocket chessGameControlerSocket){

        this.in = in;
        this.chessGameControlerSocket = chessGameControlerSocket;
    }

    public void run() {

        while(true){
            try {

                message = in.readLine();
                chessGameControlerSocket.recieveMessage(message);

            } catch (IOException e) {

                e.printStackTrace();
            }
        }
    }
}
