package tools.socket;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

/**
 * Created by milihhard on 14/06/17.in tools.socket
 */
public class CheckConnexion implements Runnable  {
    Socket socket;
    Frame frame;
    boolean isConnected;

    public CheckConnexion(Socket socket, Frame frame) {
        this.socket = socket;
        this.frame = frame;
        isConnected = true;
    }

    @Override
    public void run() {

        while (isConnected){
            try {
                socket.getInputStream();
                TimeUnit.SECONDS.sleep(2);
            } catch (IOException e) {
            isConnected = false;
                e.printStackTrace();
                System.out.println("l'autre s'est déconnecté");
                JOptionPane.showMessageDialog(frame,
                        "Eggs are not supposed to be green.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
