import model.Couleur;
import tools.socket.server.Accepter_connexion;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by milihhard on 14/06/17.in PACKAGE_NAME
 */
public class Server extends LauncherSocket {


    public Server() {
        super(Couleur.BLANC);
    }

    @Override
    Socket recieveSocket() {
        Socket s = null;
        ServerSocket ss = null;
        try {
            ss = new ServerSocket(2009);
            System.out.println("Le serveur est � l'�coute du port "+ss.getLocalPort());
            s = ss.accept();

        } catch (IOException e) {
            System.err.println("Le port "+ss.getLocalPort()+" est d�j� utilis� !");
        }

        return s;
    }
}
