package model.piece;

import model.Coord;
import model.Couleur;
import model.TypePiece;

/**
 * Created by milihhard on 12/06/17.in model.piece
 */
public class Fou extends AbstractPiece {
    public Fou(Couleur color, Coord pos) {
        super(TypePiece.FOU, color, pos);
    }

    @Override
    public boolean canTheoricalyMove(Coord pos) {
      // re-sentering piece in 0 and cheking if movment is posibol
        return pos.x - getPosX() == pos.y - getPosY() || pos.x - getPosX() == - (pos.y - getPosY());
    }

    @Override
    public Coord getNextStep(Coord posStep, Coord posFinal) {
         int x, y;
         if(posFinal.x - posStep.x < 0){
           x = posStep.x -1;
         }else{
           x = posStep.x + 1;
         }

         if(posFinal.y - posStep.y < 0){
           y = posStep.y -1;
         }else{
           y = posStep.y + 1;
         }

        return new Coord(x,y);
    }
}
