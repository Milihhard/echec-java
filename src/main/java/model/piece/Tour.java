package model.piece;

import model.Coord;
import model.Couleur;
import model.TypePiece;

/**
 * Created by milihhard on 12/06/17.in model
 */
public class Tour extends AbstractPiece{


    public Tour(Couleur color, Coord pos) {
        super(TypePiece.TOUR, color, pos);
    }

    @Override
    public boolean canTheoricalyMove(Coord pos) {
        return getPosX() == pos.x || getPosY() == pos.y;
    }

    @Override
    public Coord getNextStep(Coord posStep, Coord posFinal) {
        int posX = posFinal.x - posStep.x;
        posX = posX > 0 ? 1 : posX == 0? 0 : -1;
        int posY = posFinal.y - posStep.y;
        posY = posY > 0 ? 1 : posY == 0? 0 : -1;
        return new Coord(posStep.x + posX, posStep.y + posY);
    }
}
