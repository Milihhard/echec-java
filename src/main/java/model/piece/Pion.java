package model.piece;

import model.Coord;
import model.Couleur;
import model.TypePiece;


/**
 * Created by milihhard on 12/06/17.in model.piece
 */
public class Pion extends AbstractPiece {
    private boolean hasMoved;
    public Pion(Couleur color, Coord pos) {
        super(TypePiece.PION, color, pos);
        hasMoved = false;
    }

    @Override
    public boolean move(Coord pos) {
        hasMoved = true;
        return super.move(pos);

    }

    @Override
    public boolean canBePromoted() {
        return true;
    }

    @Override
    public boolean canTheoricalyMove(Coord pos, boolean canCatch, boolean isCastlingPossible) {
        boolean sameY = getPosX() == pos.x;
        int moveIntUp = getColor() ==  Couleur.NOIR ? pos.y - getPosY() : getPosY()-pos.y ;
        boolean moveUp = !canCatch && (moveIntUp == 1 || !hasMoved &&  moveIntUp == 2);
        boolean eat = canCatch && ( getColor() == Couleur.NOIR ? getPosY() - pos.y == -1 && Math.abs(getPosX() - pos.x) == 1 : getPosY() - pos.y == 1 && Math.abs(getPosX() - pos.x) == 1);
        return sameY && moveUp || eat;
    }

    @Override
    public boolean canTheoricalyMove(Coord pos) {
        return false;
    }

    @Override
    public Coord getNextStep(Coord posStep, Coord posFinal) {
        int moveIntUp = posStep.y-posFinal.y;
        if( Math.abs(moveIntUp) == 2){
            return new Coord(posStep.x, posStep.y - moveIntUp/2);
        } else {
            return posFinal;
        }
    }
}
