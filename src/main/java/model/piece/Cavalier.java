package model.piece;

import model.Coord;
import model.Couleur;
import model.TypePiece;

/**
 * Created by milihhard on 12/06/17.in model.piece
 */
public class Cavalier extends AbstractPiece {
    public Cavalier(Couleur color, Coord pos) {
        super(TypePiece.CAVALIER, color, pos);
    }

    @Override
    public boolean canTheoricalyMove(Coord pos) {

        return pos.x == getPosX() - 2 && pos.y == getPosY() - 1 || pos.x == getPosX() - 2 && pos.y == getPosY() + 1 || pos.x == getPosX() + 2 && pos.y == getPosY() - 1 || pos.x == getPosX() + 2 && pos.y == getPosY() + 1 || pos.y == getPosY() - 2 && pos.x == getPosX() - 1 || pos.y == getPosY() - 2 && pos.x == getPosX() + 1 || pos.y == getPosY() + 2 && pos.x == getPosX() - 1 || pos.y == getPosY() + 2 && pos.x == getPosX() + 1;
    }

    @Override
    public Coord getNextStep(Coord posStep, Coord posFinal) {
        return posFinal;
    }
}
