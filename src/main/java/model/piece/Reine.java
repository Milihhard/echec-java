package model.piece;

import model.Coord;
import model.Couleur;
import model.TypePiece;

/**
 * Created by milihhard on 12/06/17.in model.piece
 */
public class Reine extends AbstractPiece {
    public Reine(Couleur color, Coord pos) {
        super(TypePiece.REINE, color, pos);
    }

    @Override
    public boolean canTheoricalyMove(Coord pos) {
        return pos.x - getPosX() == pos.y - getPosY() || pos.x - getPosX() == - (pos.y - getPosY()) || getPosX() == pos.x || getPosY() == pos.y;
    }

    @Override
    public Coord getNextStep(Coord posStep, Coord posFinal) {
        int x, y;

        if(posFinal.x == posStep.x){
          x = posStep.x;
          if(posStep.y - posFinal.y > 0){
            y = posStep.y - 1;
          }else{
            y = posStep.y + 1;
          }
        }else if(posFinal.y == posStep.y){
          y = posStep.y;
          if(posStep.x - posFinal.x > 0){
            x = posStep.x - 1;
          }else{
            x = posStep.x + 1;
          }
        }else{
          if(posFinal.x - posStep.x < 0){
            x = posStep.x -1;
          }else{
            x = posStep.x + 1;
          }
          if(posFinal.y - posStep.y < 0){
            y = posStep.y -1;
          }else{
            y = posStep.y + 1;
          }
        }

        return new Coord(x,y);
    }
}
