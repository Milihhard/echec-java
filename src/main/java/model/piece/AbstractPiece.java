package model.piece;

import model.Coord;
import model.Couleur;
import model.TypePiece;

/**
 * Created by milihhard on 12/06/17.in model
 */
public abstract class AbstractPiece implements Pieces {

    private TypePiece type;
    private Couleur color;
    private Coord pos;

    public AbstractPiece(TypePiece type, Couleur color, Coord pos) {
        this.type = type;
        this.color = color;
        this.pos = pos;
    }

    @Override
    public TypePiece getType() {
        return type;
    }

    @Override
    public String toString(){
        return type.toString();
    }

    @Override
    public Couleur getColor() {
        return color;
    }

    @Override
    public Coord getPos() {
        return pos;
    }

    @Override
    public int getPosX() {
        return pos.x;
    }

    @Override
    public int getPosY() {
        return pos.y;
    }

    @Override
    public boolean move(Coord pos){
        this.pos = pos;
        return true;
    }

    @Override
    public boolean canTheoricalyMove(Coord pos, boolean canCatch, boolean isCastlingPossible){
       return canTheoricalyMove(pos);
    }


    public abstract boolean canTheoricalyMove(Coord pos);

    @Override
    public abstract Coord getNextStep(Coord posStep, Coord posFinal);

    @Override
    public boolean canBePromoted() {
        return false;
    }
}
