package model.piece;

import model.Coord;
import model.Couleur;
import model.TypePiece;

/**
 * Created by milihhard on 12/06/17.in model.piece
 */
public class Roi extends AbstractPiece {
    public Roi(Couleur color, Coord pos) {
        super(TypePiece.ROI, color, pos);
    }


    @Override
    public boolean canTheoricalyMove(Coord pos) {

        if (pos.x - getPosX() <= 1 && pos.x - getPosX() >= -1 && pos.y - getPosY() <= 1 && pos.y - getPosY() >= -1)
            return pos.x - getPosX() == pos.y - getPosY() || pos.x - getPosX() == -(pos.y - getPosY()) || getPosX() == pos.x || getPosY() == pos.y;
        else {
            return false;
        }
    }

    @Override
    public Coord getNextStep(Coord posStep, Coord posFinal) {
        return posFinal;
    }
}
