package model.piece;

import model.Coord;
import model.Couleur;
import model.TypePiece;

/**
 * Created by milihhard on 12/06/17.in model
 */
public interface Pieces {
    TypePiece getType();

    Couleur getColor();

    Coord getPos();

    int getPosX();

    int getPosY();

    //déplace la pièce
    boolean move(Coord pos);

    //vérifie que la pièce peut se déplacer sans prendre compte des autres pièce
    boolean canTheoricalyMove(Coord pos, boolean canCatch, boolean isCastlingPossible);

    //donne la prochaine étape de la pièce
    Coord getNextStep(Coord posStep, Coord posFinal);

    boolean canBePromoted();

}
