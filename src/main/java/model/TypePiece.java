package model;

/**
 * Created by milihhard on 12/06/17.in model
 */
public enum TypePiece {
    TOUR, PION, CAVALIER, FOU, REINE, ROI;


    @Override
    public String toString() {
        switch (this) {
            case TOUR:
                return "Tour";
            case PION:
                return "Pion";
            case CAVALIER:
                return "Cavalier";
            case FOU:
                return "Fou";
            case REINE:
                return "Reine";
            case ROI:
                return "Roi";
            default:
                return "Error";
        }
    }
}
