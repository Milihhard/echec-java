package model;

import model.piece.Pieces;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by milihhard on 12/06/17.in model
 */
public class Echiquier {


    String message;
    Map<Couleur, Jeu> jeux;
    Couleur joueur;

    public Echiquier() {
        jeux = new TreeMap<Couleur, Jeu>();
        jeux.put(Couleur.BLANC ,new Jeu(Couleur.BLANC));
        jeux.put(Couleur.NOIR ,new Jeu(Couleur.NOIR));
        joueur = Couleur.BLANC;
        setMessage("Début de la partie.\n" + showActualJoueur());
    }

    public Couleur getColorCurrentPlayer() {
        return joueur;
    }

    public String getMessage() {
        return message;
    }

    private void setMessage(String message){this.message = message;}

    public Couleur getPieceColor(int x, int y) {
        Couleur col = jeux.get(Couleur.BLANC).getPieceColor(x,y);
        if(col == Couleur.NOIRBLANC)
            col = jeux.get(Couleur.NOIR).getPieceColor(x,y);
        return col;
    }

    public List<PieceIHMs> getPiecesIHM() {
        List<PieceIHMs> pieces= jeux.get(Couleur.BLANC).getPiecesIHM();
        pieces.addAll(jeux.get(Couleur.NOIR).getPiecesIHM());
        return pieces;
    }

    public boolean isEnd() {
        return false;
    }

    public boolean isMoveOk(int xInit, int yInit, int xFinal, int yFinal) {
        return ! new Coord(xInit, yInit).equals(new Coord(xFinal, yFinal)) && isMoveTheoricOk(xInit, yInit, xFinal, yFinal)  && isMoveWithoutObstacle(xInit, yInit, xFinal, yFinal);

    }

    //regarden si on peut y aller et qu'il y a pas d'allié
    public boolean isMoveTheoricOk(int xInit, int yInit, int xFinal, int yFinal){

        return jeux.get(getColorCurrentPlayer()).isMoveOk( xInit, yInit, xFinal, yFinal, jeux.get(getColorPlayerNotPlaying()).isPieceHere(xFinal,yFinal),true) &&
                !jeux.get(getColorCurrentPlayer()).isPieceHere(xFinal, yFinal);
    }
    public boolean isMoveWithoutObstacle(int xInit, int yInit, int xFinal, int yFinal){

        //Todo
        return jeux.get(getColorCurrentPlayer()).isColisonOk(xInit, yInit, xFinal, yFinal)
                && isNotEnemyTrajectory(xInit, yInit, xFinal, yFinal);
    }

    private boolean isNotEnemyTrajectory(int xInit, int yInit, int xFinal, int yFinal){
        Coord step = new Coord(xInit, yInit);
        Coord finalStep = new Coord(xFinal, yFinal);
        do{
            step = jeux.get(getColorCurrentPlayer()).getNextStep(xInit, yInit,step.x,step.y,finalStep.x,finalStep.y);
        } while(step != null && !finalStep.equals(step) && !jeux.get(getColorPlayerNotPlaying()).isPieceHere(step.x, step.y));
        if(finalStep.equals(step))
            return true;
        else
            return false;
    }

    public boolean move(int xInit, int yInit, int xFinal, int yFinal) {
        if (jeux.get(getColorPlayerNotPlaying()).capture(xFinal,yFinal)){

        }
        return jeux.get(getColorCurrentPlayer()).move( xInit, yInit, xFinal, yFinal);
    }

    public List<Coord> getPossibleMove(Coord initCoord){
        List<Coord> coords = new ArrayList<>();
        for(int y = 0; y <8; y++){
            for(int x = 0; x < 8; x ++){
                Coord coord = new Coord(x,y);
                if(this.isMoveOk(initCoord.x, initCoord.y, x,y)){
                    coords.add(coord);
                }
            }
        }
        return coords;

    }


    public void switchJoueur() {
        joueur = (joueur.equals(Couleur.NOIR)) ? Couleur.BLANC : Couleur.NOIR;
    }
    private Couleur getColorPlayerNotPlaying(){
        return joueur.equals(Couleur.NOIR) ? Couleur.BLANC : Couleur.NOIR;
    }

    private PieceIHM getPieceIHM(int x, int y){
        List<PieceIHMs> pieces = getPiecesIHM();
        for (PieceIHMs piece : pieces){
            if(piece.getX() == x && piece.getY() == y){
                return (PieceIHM) piece;
            }
        }
        return null;
    }

    public String toString() {
        String string=null;
        for (Couleur c : jeux.keySet()){
            string += jeux.get(c).toString();
        }

        return string;
    }
    private String showActualJoueur(){
        return "Au joueur " + getColorCurrentPlayer() + " de jouer.";
    }
    private String showNextJoueur(){
        return "Au joueur " + getColorPlayerNotPlaying() + " de jouer.";
    }
}
