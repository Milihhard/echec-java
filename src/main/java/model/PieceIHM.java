package model;

import model.piece.Pieces;

/**
 * @author francoise.perrin
 * 
 * Classe qui permet de retourner des informations sur les pièces
 * en view d'une utilisation par une IHM
 * 
 * DP Adapter
 * 
 */
public  class PieceIHM  implements PieceIHMs {
	 
	private Pieces piece;
	
	public PieceIHM(Pieces piece) {
		this.piece = piece;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PieceIHM [name=" + getNamePiece() + ", couleur=" + getCouleur() + ", x="
				+ getX() + ", y=" + getY() + "]";
	}



	@Override
	public int getX() {
		return piece.getPosX();
	}



	@Override
	public int getY() {
		return piece.getPosY();
	}



	@Override
	public String getNamePiece() {
		return piece.toString();
	}

	@Override
	public TypePiece getTypePiece() {
		return piece.getType();
	}


	@Override
	public Couleur getCouleur() {
		return piece.getColor();
	}

}