package model;

import model.piece.Pieces;
import model.piece.Reine;
import tools.ChessPiecesFactory;
import tools.ChessSinglePieceFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by milihhard on 12/06/17.in model
 */
public class Jeu implements Game {

    private Couleur color;
    private List<Pieces> pieces;

    public Jeu(Couleur color) {
        pieces = new ArrayList<>();
        this.color = color;
        List<Pieces> piecesFactory = ChessPiecesFactory.newPieces(color);
        for (Pieces pieceFactory : piecesFactory) {
            pieces.add(pieceFactory);
        }
    }

    @Override
    public boolean isPieceHere(int x, int y) {
        for (Pieces piece : pieces) {
            if (piece.getPosX() == x && piece.getPosY() == y) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isMoveOk(int xInit, int yInit, int xFinal, int yFinal, boolean isCatchOk, boolean isCastlingPossible) {
        Pieces piece = getPiece(xInit, yInit);
        if (piece != null)
            return piece.canTheoricalyMove(new Coord(xFinal, yFinal), isCatchOk, isCastlingPossible);
        return false;
    }

    public boolean isColisonOk(int xInit, int yInit, int xFinal, int yFinal) {
        Pieces piece = getPiece(xInit, yInit);

        Coord step = new Coord(xInit, yInit);
        Coord finalPos = new Coord(xFinal, yFinal);
        do {
            step = piece.getNextStep(step, finalPos);
        } while (!step.equals(finalPos) && getPiece(step.x, step.y) == null);
        if (step.equals(finalPos))
            return true;
        else
            return false;
    }

    public Coord getNextStep(int xInit, int yInit, int xStep, int yStep, int xFinal, int yFinal){
        Coord c = null;
        Pieces p = getPiece(xInit,yInit);
        if(p != null){
            c = p.getNextStep(new Coord(xStep, yStep), new Coord(xFinal, yFinal));
        }
        return c;
    }

    @Override
    public boolean move(int xInit, int yInit, int xFinal, int yFinal) {
        Pieces piece = getPiece(xInit, yInit);
        if (piece != null) {
            if (piece.canBePromoted() && (yFinal == 7 || yFinal == 0)) {
                pieces.remove(piece);
                pieces.add(ChessSinglePieceFactory.newPiece(getCouleur(), TypePiece.REINE, xInit, yInit));
                piece = getPiece(xInit, yInit);
            }
            return piece.move(new Coord(xFinal, yFinal));
        }
        return false;
    }

    @Override
    public boolean capture(int xCatch, int yCatch) {
        if (isPieceHere(xCatch, yCatch)) {
            pieces.remove(getPiece(xCatch, yCatch));
            return true;
        }
        return false;
    }

    Couleur getCouleur() {
        return color;
    }

    Couleur getPieceColor(int x, int y) {
        Pieces piece = getPiece(x, y);
        if (piece != null)
            return piece.getColor();
        return Couleur.NOIRBLANC;
    }

    java.lang.String getPieceName(int x, int y) {
        Pieces piece = getPiece(x, y);
        if (piece != null)
            return piece.toString();
        return null;
    }

    List<PieceIHMs> getPiecesIHM() {
        List<PieceIHMs> pieceIHMs = new ArrayList<>();
        for (Pieces piece : pieces) {
            pieceIHMs.add(new PieceIHM(piece));
        }
        return pieceIHMs;
    }

    void setCastling() {

    }

    void setPossibleCapture() {

    }

    @Override
    public String toString() {
        String string = null;
        for (Pieces p : pieces) {
            string += p.toString();
        }
        return string;
    }

    private Pieces getPiece(int x, int y) {
        for (Pieces piece : pieces) {
            if (piece.getPosX() == x && piece.getPosY() == y) {
                return piece;
            }
        }
        return null;
    }

}
